% demo_LDSgaussian_testParamTransform.m
%
% Sample from a latent Gaussian linear dynamical system (LDS) model, then
% test that linearly transforming the params so C is the identity gives the
% same value of the log-likelihood.

% Basic equations:
% -----------------
% X_t = A*X_{t-1} + eps_x,  eps_x ~ N(0,Q)   % latents
% Y_t = C*X_t + eps_y,      eps_y ~ N(0,R);  % observations

setpath;

% Set dimensions
nz = 10;  % dimensionality of latent z
ny = nz; % dimensionality of observation y
nT = 200; % number of time steps

% Generate random stable A
A = randn(nz);
[u,s] = eig(A,'vector'); % get eigenvectors and eigenvals
s = s/max(abs(s))*.98; % set largest eigenvalue to lie inside unit circle (enforcing stability)
s(real(s)<0) = -s(real(s)<0); % set real parts to be positive (encouraging smoothness)
A = real(u*(diag(s)/u));  % reconstruct A from eigs and eigenvectors

% Set observation matrix C
C = 0.5*randn(ny,nz); % loading weights

% Dynamics noise covariance
Q = randn(nz); Q = .1*(Q'*Q+eye(nz)); % dynamics noise covariance
R = diag(1*rand(ny,1)+.1); %  Y noise covariance
Q0 = 4*eye(nz);


%% Sample data from LDS model

mmtrue = struct('A',A,'C',C,'Q',Q,'R',R,'Q0',Q0);  % make param struct
[yy,zz] = sampleLDSgauss(mmtrue,nT); % sample from model

% Make data structure for fitting
dd = struct('yy',yy); 


%% Compute latents and log-marginal likelihood given true params

% Method 1: Run Kalman Filter-Smoother (slower traditional version)
[zzmu1,loglitrue1] = runKalmanSmooth(dd,mmtrue);

%% Transform params so C is the identity

Wtr = C;  % linear transformation of latents
Anew = Wtr*A*inv(Wtr);  % new A
Cnew = C*inv(Wtr); % new C
Qnew = Wtr*Q*Wtr'; % new Q
Q0new = Wtr*Q0*Wtr'; % new Q0

mmnew = struct('A',Anew,'C',Cnew,'Q',Qnew,'R',R,'Q0',Q0new);
% Method 1: Run Kalman Filter-Smoother (slower traditional version)
[zzmu2,loglitrue2] = runKalmanSmooth(dd,mmnew);

fprintf('Difference in log-likelihood:  %e\n',loglitrue1-loglitrue2);





