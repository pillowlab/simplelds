% test_LDSgaussian_fastKSandLogli.m
%
% Unit tests for fast Kalman Smoothing and computation of log-likelihood

addpath ../inference/
addpath ../utils
addpath ..

% Set dimensions
nz = 25;  % dimensionality of latent z
ny = 25; % dimensionality of observation y
nT = 100; % number of time steps
nu = 10; % number of input dimensions

% Set model parameters
% --------------------

% Set dynamics matrix A
A = randn(nz);
[u,s] = eig(A,'vector'); % get eigenvectors and eigenvals
s = s/max(abs(s))*.98; % set largest eigenvalue to lie inside unit circle (enforcing stability)
s(real(s)<0) = -s(real(s)<0); % set real parts to be positive (encouraging smoothness)
A = real(u*(diag(s)/u));  % reconstruct A from eigs and eigenvectors

% Set observation matrix C
C = 0.5*randn(ny,nz); % loading weights

% Set input matrices B and D
B = 0.5*randn(nz,nu);  % weights from inputs to latents
D = 0.5*randn(ny,nu);  % weights from inputs to observed

% Dynamics noise covariance
Q = randn(nz); Q = .1*(Q'*Q+eye(nz)); % dynamics noise covariance
R = diag(1*rand(ny,1)+.1); %  Y noise covariance
Q0 = 4*eye(nz);

% Use discrete Lyapunov equation solver to compute asymptotic covariance
Pinf = dlyap(A,Q);


%% Sample data from LDS model

mmtrue = struct('A',A,'B',B,'C',C,'D',D,'Q',Q,'R',R,'Q0',Q0);  % make param struct
uu = randn(nu,nT); % sample inputs 
[yy,zz] = sampleLDSgauss(mmtrue,nT,uu); % sample from model

% Make data structure for fitting
dd = struct('yy',yy,'uu',uu); 

%% Compute latents and log-marginal likelihood given true params

% Method 1: Run Kalman Filter-Smoother (slower traditional version)
%fprintf('runKalmanSmooth:\n')
[zzmu1,logli1,zzcov1,zzcov_diag1] = runKalmanSmooth(dd,mmtrue);

% Method 2: Run Kalman Filter-Smoother (fast traditional version)
%fprintf('runKalmanSmooth_fast:\n')
[zzmu2,logli2,zzcov2,zzcov_diag2] = runKalmanSmooth_fast(dd,mmtrue); % run Kalman Filter-Smoother

% Method 3: Run Kalman Filter-Smoother (fast light-memory version)
%fprintf('runKalmanSmooth_fastlight:\n')
[zzmu3,logli3,Sig1,SigT,SigDiagSum,SigOffDiagSum,ttconvrg] = runKalmanSmooth_fastlight(dd,mmtrue); % run Kalman Filter-Smoother

%% Compute log-marginal likelihood only 

%fprintf('compLogMargLi_LDSgaussian:\n')
logli4 = compLogMargLi_LDSgaussian(dd,mmtrue);

%% Now do some checks to make sure the methods agree

% Check log marginal likelihood computation
loglis = [logli1, logli2,logli3,logli4];
if max(loglis)-min(loglis) > abs(mean(loglis))*1e-10
    LogLiTEST=0;
    warning('UNIT TEST FAILED:log-likelihoods do not agree');
else
    fprintf('UNIT TEST PASSED:log-likelihoods agree\n');
    LogLiTEST=1;
end

% Check posterior mean (from Kalman Smoothing)
meanCheck = [max(abs(zzmu1(:)-zzmu2(:))), max(abs(zzmu1(:)-zzmu2(:)))];
if any(meanCheck>mean(abs(zzmu1(:)))*1e-10)
    MeanTEST=0;
    warning('UNIT TEST FAILED: latent means do not agree');
else
    fprintf('UNIT TEST PASSED: means agree\n');
    MeanTEST=1;
end

% Check posterior covariance (from Kalman Smoothing)
covCheck = [max(abs(zzcov1(:)-zzcov2(:))), max(abs(zzcov_diag1(:)-zzcov_diag2(:)))];
if any(covCheck>1e-12)
    CovTEST=0;
    warning('UNIT TEST FAILED: latent covariances do not agree');
else
    fprintf('UNIT TEST PASSED: covariances agree\n');
    CovTEST=1;
end

% Check summed covariances computed by 'fastlight' code
Sig1a = zzcov1(:,:,1);
SigTa = zzcov1(:,:,nT);
SigSuma = sum(zzcov1,3);
SigOffDiagSuma = sum(zzcov_diag1,3);

SumCovCheck = [max(abs((Sig1a(:)-Sig1(:)))),max(abs(SigTa(:)-SigT(:))), ...
    max(abs(SigSuma(:)-SigDiagSum(:))), max(abs(SigOffDiagSuma(:)-SigOffDiagSum(:)))];
if any(SumCovCheck > max(abs(SigDiagSum(:)))*1e-10)
    SumCovTEST=0;
    warning('UNIT TEST FAILED: summed covariances do not agree');
else
    fprintf('UNIT TEST PASSED: summed covariances agree\n');
    SumCovTEST=1;
end

% Report whether all tests passed
if all([LogLiTEST, MeanTEST, CovTEST,SumCovTEST])
    fprintf('test_LDSgaussian_fastKSandLogli.m test PASSED\n'); 
else
    [LogLiTEST, MeanTEST, CovTEST,SumCovTEST]
    warning('test_LDSgaussian_fastKSandLogli.m test FAILED: logli, mean, or cov mismatch'); 
end
