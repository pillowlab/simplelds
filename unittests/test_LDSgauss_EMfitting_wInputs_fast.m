% test_LDSgaussian_EMfitting_wInputs_fast.m
%
% Test that fast EM fitting agrees with original (slow) EM fitting, with
% inputs

addpath ../inference/
addpath ../utils
addpath ..

% Set dimensions
nz = 5;  % dimensionality of latent z
ny = 8; % dimensionality of observation y
nT = 500; % number of time steps
nu = 5;  % number of inputs

% Set model parameters
% --------------------

% Set dynamics matrix A
A = randn(nz);
[u,s] = eig(A,'vector'); % get eigenvectors and eigenvals
s = s/max(abs(s))*.98; % set largest eigenvalue to lie inside unit circle (enforcing stability)
s(real(s)<0) = -s(real(s)<0); % set real parts to be positive (encouraging smoothness)
A = real(u*(diag(s)/u));  % reconstruct A

% Set observation matrix C
C = 0.5*randn(ny,nz); % loading weights

% Dynamics noise covariance
Q = randn(nz); Q = .1*(Q'*Q+eye(nz)); % dynamics noise covariance
Q0 = 1.5*eye(nz); % initial covariance
R = .25*diag(1*rand(ny,1)+.1); %  Y noise covariance

% Set input matrices B and D
B = 0.5*randn(nz,nu);  % weights from inputs to latents
D = 0.5*randn(ny,nu);  % weights from inputs to observed

%% Sample data from LDS model

uu = randn(nu,nT); % external inputs

mmtrue = struct('A',A,'B',B,'C',C,'D',D,'Q',Q,'R',R,'Q0',Q0);  % make param struct
[yy,zz] = sampleLDSgauss(mmtrue,nT,uu); % sample from model

% Make data structure for fitting
dd = struct('yy',yy,'uu',uu); 

%% Compute ML estimate for model params using EM

% Set options for EM     
optsEM.maxiter = 5;    % maximum # of iterations
optsEM.dlogptol = 1e-3;  % stopping tolerance
optsEM.display = inf;  % display frequency


% Set set of parameters to params to learn
updateSettings = [eye(6),[1;1;0;0;0;0],[1;1;0;0;1;0],...
    [0;0;1;1;0;0],[0;0;1;1;0;1], ones(6,1)];
nSettings = size(updateSettings,2);

loglis_fit1 = zeros(1,nSettings);
loglis_fit2 = zeros(1,nSettings);
logliFinal1 = zeros(1,nSettings);
logliFinal2 = zeros(1,nSettings);

% Initialize fitting struct
A0 = eye(nz)*.1+randn(nz)*.01; % initial A param
B0 = randn(nz,nu)*.01; % initial A param
C0 = randn(ny,nz)*.1; % initial C param
D0 = randn(ny,nu)*.01; % initial A param
Q00 = Q*2;  % initial Q param
R0 = R*2;  % initial R param
mm0 = struct('A',A0,'B',B0,'C',C0,'D',D0,'Q',Q00,'R',R0,'Q0',Q0);  % make struct with initial params

% Loop over different settings of params to fit
for jj = 1:nSettings
    % Specify which parameters to learn. (Set to '0' or 'false' to NOT update).
    optsEM.update.A = updateSettings(1,jj);
    optsEM.update.C = updateSettings(2,jj);
    optsEM.update.Q = updateSettings(3,jj);
    optsEM.update.R = updateSettings(4,jj);

    % Run EM inference using standard code
    [mm1,logli1,logliTrace1] = runEM_LDSgaussian(dd,mm0,optsEM);
    loglis_fit1(jj) = logli1;
    logliFinal1(jj) = logliTrace1(end);

    % Run EM inference using standard code
    [mm2,logli2,logliTrace2] = runEM_LDSgaussian_fast(dd,mm0,optsEM);
    loglis_fit2(jj) = logli2;
    logliFinal2(jj) = logliTrace2(end);

end

lldiffs = abs(loglis_fit1 - loglis_fit2);
llfinaldiffs = abs(logliFinal1-logliFinal2);

%% Check if tests passed

if any(lldiffs >max(abs(loglis_fit1))*1e-10)
    warning('test_LDSgauss_EMfitting_fast.m test FAILED: final log-lis don''t match');
else
    fprintf('test_LDSgauss_EMfitting_fast.m test PASSED: log-lis match\n');
end

if any(llfinaldiffs > max(abs(logliFinal2))*1e-10)
    fprintf('test_LDSgauss_EMfitting_fast.m test FAILED: log-li traces don''t match\n');
else
    fprintf('test_LDSgauss_EMfitting_fast.m test PASSED: log-li traces match\n');
end

