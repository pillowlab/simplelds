function mmtarg = alignLDSmodels(zz1,zztarg,mm1)
% mmtarg = alignLDSmodels(zz1,zztarg,mm1)
%
% Align two LDS models by performing a linear transformation of one set of
% latents to best match (in a least-squares sense) another set of latents.
%
% INPUTS
% ------
%    zz1 [nz x nT] - set of latents from model 1
% zztarg [nz x nT] - set of latents from a target model
%    mm1 [struct]  - model struct for latent LDS model, with possible fields:
%            .A [nz x nz] - dynamics matrix
%            .B [nz x ns] - input matrix (optional)
%            .C [ny x nz] - latents-to-observations matrix
%            .D [ny x ns] - input-to-observations matrix (optional)
%            .Q [nz x nz] - latent noise covariance
%            .R [nz x nz] - observed noise covariance
%
% OUTPUTS
% ------
%   mmtarg - new struct with params aligned as best as possible to match zztarg


mmtarg = mm1; % initialize

Walign = ((zz1*zz1')\(zz1*zztarg'))'; % alignment weights

% Transform inferred params to align with true params

if isfield(mmtarg,'A') && ~isempty(mmtarg.A)
    mmtarg.A = Walign*mm1.A/Walign;  % transform dynamics
end

if isfield(mmtarg,'B') && ~isempty(mmtarg.B)
    mmtarg.B = Walign*mm1.B;     % transform input weights
end

if isfield(mmtarg,'C') && ~isempty(mmtarg.C)
    mmtarg.C = mm1.C/Walign;     % transform projection weights
end

% transform latent noise covariances
mmtarg.Q = Walign*mm1.Q*Walign';   % transform latent noise covariance
mmtarg.Q0 = Walign*mm1.Q0*Walign'; % initial covariance
