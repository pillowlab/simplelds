function L = logmvnpdf(X,mu,C)
% L = logmvnpdf(X,mu,C)
%
% Evaluates log of mulivariate normal pdf with mean mu and covariance C 
% for each row of X
%
% Inputs
% -------
%   X [n x m] - data
%  mu [1 x m] - mean
%   C [m x m] - covariance
%
% Outputs
% -------
%   L [n x 1] - log of normal pdf for each row of X

mx = size(C,1);


% Check if mu is correct size 
if size(mu,2)~=mx
    if (size(mu,1)==mx)
        mu = mu'; % take transpose if necessary
    else
        error('LOGMVNPDF: mean mu doesn''t match size of data X')
    end
end

% Compute log-determinant term
logdettrm = -.5*logdet(2*pi*C);

% Compute quadratic term
Xctr = X-mu; % centered X
Qtrm = -.5*sum(Xctr.*(Xctr/C),2);

% Sum two terms to get log pdf
L = Qtrm+logdettrm;

