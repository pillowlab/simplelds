function checkLDSparams(mm)
% checkLDSparams(mm);
%
% Checks if A matrix is stable, if Q, Q0, and R (if present) are positive
% definite. 

% Check dynamics matrix A
if max(abs(eig(mm.A)))>1.1
    warning('Dynamics matrix A is unstable!\n  Largest eigenvalue:  %.3f', max(abs(eig(mm.A))));
    fprintf('\n');
end

% Check latent covariance Q0
try 
    chol(mm.Q0);
catch
    error('Initial covariance Q0 is not positive definite');
end
    
% Check latent covariance Q
try 
    chol(mm.Q);
catch
    error('Covariance Q is not positive definite');
end
    
% Check observed covariance R
if isfield(mm,'R')
    try
        chol(mm.R);
    catch
        error('Covariance R is not positive definite');
    end
end



    
