function [zzmu,logli,zzcov,zzcov_d1] = runKalmanSmooth_fast(dd,mm)
% [zzmu,logli,zzcov,zzcov_d1] = runKalmanSmooth_fast(dd,mm)
%
% Run Kalman Filter-Smoother for latent LDS Gaussian model using fast
% method that skips updating the covariance once it has converged. 
%
% Note: requires that A matrix is stable (has max |eigenvalue| <= 1)
%
% INPUTS:
% -------
%  dd [struct] - data structure with observations & inputs
%           .yy [ny x nT] - observations (aka emissions)
%           .uu [ns x nT] - inputs
%
%  mm  [struct] - model struct with fields
%           .A [nz x nz] - dynamics matrix
%           .B [nz x ns] - input matrix (optional)
%           .C [ny x nz] - latents-to-observations matrix
%           .D [ny x ns] - input-to-observations matrix (optional)
%           .Q [nz x nz] - latent noise covariance
%           .R [ny x ny] - observed noise covariance
%           .Q0 [nz x nz] - latent noise covariance for 1st time step
%
% OUTPUTS:
% --------
%      zzmu [nz x nT]      - posterior mean latents zz | yy
%     logli [1 x 1]        - log-likelihood P( yy | ss, theta )
%     zzcov [nz x nz x nT] - marginal cov for time bins 0 to T
%  zzcov_d1 [nz x nz x nT-1]   - above-diagonal cov for bins 1 to T
%
%
% Basic equations:
% -----------------
% X_t = A*X_{t-1} + w_t,    w_t ~ N(0,Q)   % latent dynamics
% Y_t = C*X_t     + v_t,    v_t ~ N(0,R);  % observations


% Check that LDS model params are valid (covs are psd)
checkLDSparams(mm);

% % Check that dynamics matrix is stable
% if max(abs(eig(mm.A)))>1
%     warning('Unstable dynamics matrix: largest eigenvalue of A matrix = %f\n', max(abs(eig(mm.A))));
% end

% Extract sizes
nz = size(mm.A,1);  % number of obs and latent dimensions
[ny,nT] = size(dd.yy); % number of obs and time bins

% pre-compute C'*inv(R) and C'*inv(R)*C;
CtRinv = mm.C'/mm.R;
CtRinvC = CtRinv*mm.C;

% check if input-latent matrix is provided
if isfield(mm,'B') && ~isempty(mm.B) 
    zin = mm.B*dd.uu;  % additive intput to latents (as column vectors)
else
    zin = zeros(nz,nT);
end

% check if intput-obs matrix is provided
if isfield(mm,'D') && ~isempty(mm.D) 
    yyctr = dd.yy-(mm.D*dd.uu);  % subtract additive intput to observations
else
    yyctr = dd.yy;
end

% Allocate storage
zzmu = zeros(nz,nT);   % posterior mean E[ zz(t) | Y]
zzcov = zeros(nz,nz,nT); % marginal cov of zz(t)
munext = zeros(nz,nT);   % prior mean for next step:  E[ z(t) | y(1:t-1)]
Pnext = zeros(nz,nz,nT); % prior cov for next step: cov[ z(t) | y(1:t-1)]
logcy = zeros(1,nT); % store conditionals P(y(t) | y(1:t))

% ============================================
% Kalman Filter (forward pass through data)
% ============================================

% Process time bin 1
% ------------------
% 1. Predict
Pnext(:,:,1) = mm.Q0;   % prior cov for time bin 1
munext(:,1) = zeros(nz,1)+zin(:,1);  % prior mean for time bin 1
% 2. Update
zzcov(:,:,1) = inv(inv(Pnext(:,:,1))+CtRinvC); % KF posterior cov for time bin 1 
zzmu(:,1) = zzcov(:,:,1)*(CtRinv*yyctr(:,1) + (Pnext(:,:,1)\munext(:,1))); % KF mean for time bin 1
% 3. Compute marginal likelihood for bin 1
SigY = mm.C*Pnext(:,:,1)*mm.C'+mm.R; % conditional Y cov
logcy(1) = logmvnpdf(yyctr(:,1)',(mm.C*munext(:,1))',SigY); % marginal log P(y_1 )


% --------------------
% Process initial time bins (where cov is changing)
COVCONVRG = 0; % Boolean for when covariance has converged to its asymptotic value
tt = 1; % index variable
ttconvrg = nT; % initialize variable where convergence occurred
while (tt<nT) && ~COVCONVRG
    tt = tt+1;  % time index for latents (which started at 0)
    
    % Step 1 (Predict):
    Pnext(:,:,tt) = mm.A*zzcov(:,:,tt-1)*mm.A' + mm.Q; % prior cov for time bin t
    munext(:,tt) = mm.A*zzmu(:,tt-1)+zin(:,tt); % prior mean for time bin t
    % Step 2 (Update):
    zzcov(:,:,tt) = inv(inv(Pnext(:,:,tt))+CtRinvC);   % KF cov for time bin t
    zzmu(:,tt) = zzcov(:,:,tt)*(CtRinv*yyctr(:,tt) + (Pnext(:,:,tt)\munext(:,tt))); % KF mean
    
    % compute log P(y_t | y_{1:t-1})
    SigY = mm.C*Pnext(:,:,tt)*mm.C'+mm.R; % conditional Y cov
    logcy(tt) = logmvnpdf(yyctr(:,tt)',(mm.C*munext(:,tt))',SigY); % conditional log prob
    
    if sum(abs(diag(zzcov(:,:,tt))-diag(zzcov(:,:,tt-1))))<1e-12
        COVCONVRG = 1; % Boolean to indicate convergence
        ttconvrg = tt; % time index where covariance has converged
    end
end

% --------------------
% Process remaining time bins (where cov is fixed)
Pnextinv = Pnext(:,:,tt)\eye(nz);  % inverse of Pnext
SigY = mm.C*Pnext(:,:,tt)*mm.C'+mm.R; % covariance of observations
SigYlogdet = -0.5*logdet(2*pi*SigY); % log-determinant term in marginal likelihood
SigYinv = SigY\eye(ny); % inverse of marginal likelihood covariance

for tt = (ttconvrg+1):(nT+1)
    tt = tt-1; % time index for inputs and outputs (zin and yyctr)

    % Step 1 (Predict):
    Pnext(:,:,tt) = Pnext(:,:,tt-1); % prior cov for time bin t
    munext(:,tt) = mm.A*zzmu(:,tt-1)+zin(:,tt); % prior mean for time bin t
    % Step 2 (Update):
    zzcov(:,:,tt) = zzcov(:,:,tt-1);   % KF cov for time bin t
    zzmu(:,tt) = zzcov(:,:,tt)*(CtRinv*yyctr(:,tt) + (Pnextinv*munext(:,tt))); % KF mean
    
    % compute log P(y_t | y_{1:t-1})
    % %% old, slow: logcy(tty) = logmvnpdf(yyctr(:,tty)',(mm.C*munext(:,tt))',SigY); %%
    ymu = yyctr(:,tt)-(mm.C*munext(:,tt));  % mean
    logcy(tt) = SigYlogdet - 0.5*(sum(ymu.*(SigYinv*ymu))); % fast logmvnpdf 
end

% compute marginal log-likelihood P(y | theta)
logli = sum(logcy);

% ============================================
% Kalman Smoother (backward pass)
% ============================================

if nargout > 3
    zzcov_d1 = zeros(nz,nz,nT-1); % above-diagonal covariance block
end

if (ttconvrg > nT/2)
    % ---------------------------------------------------
    % Run regular  (slow) backward Kalman smoother pass    
    % ---------------------------------------------------

    warning('Warning: cov did not converge in time for fast method to be helpful (ttconvrg = %d)',ttconvrg);

    % Run Kalman smoother for time bins nT-1 to 1
    for tt = (nT-1):-1:1
        Jt = (zzcov(:,:,tt)*mm.A')/Pnext(:,:,tt+1); % matrix we need
        zzcov(:,:,tt) = zzcov(:,:,tt) + Jt*(zzcov(:,:,tt+1)-Pnext(:,:,tt+1))*Jt'; % update cov
        zzmu(:,tt) = zzmu(:,tt) + Jt*(zzmu(:,tt+1)-munext(:,tt+1)); % update mean

        if nargout > 3
            zzcov_d1(:,:,tt) = Jt*zzcov(:,:,tt+1); % compute off-diagonal cov block
        end
    end
    
else  
    % ---------------------------------------------------
    % Run fast version, skipping updating cov updates in middle block
    % ---------------------------------------------------
    
    % --------------------
    % Run Kalman smoother for time bins nT-1 to nT-ttconverg
    %  (final time bins where cov IS changing)
    Jt = (zzcov(:,:,nT-1)*mm.A')/Pnext(:,:,nT); % matrix we need
    for tt = (nT-1):-1:(nT-ttconvrg)
        zzcov(:,:,tt) = zzcov(:,:,tt) + Jt*(zzcov(:,:,tt+1)-Pnext(:,:,tt+1))*Jt'; % update cov
        zzmu(:,tt) = zzmu(:,tt) + Jt*(zzmu(:,tt+1)-munext(:,tt+1)); % update mean
        
        if nargout > 3
            zzcov_d1(:,:,tt) = Jt*zzcov(:,:,tt+1);
        end
    end
    
    % --------------------
    % Run Kalman smoother for time bins nT-ttconvrg to ttconverg
    %  (Middle time bins where cov isn't changing)
    zzcov_convrg = zzcov(:,:,nT-ttconvrg); % converged cov
    zzcov_offdiag_convrg = Jt*zzcov_convrg; % converged off-diagonal cov
    for tt = (nT-ttconvrg-1):-1:(ttconvrg+1)
        zzcov(:,:,tt) = zzcov_convrg;
        zzmu(:,tt) = zzmu(:,tt) + Jt*(zzmu(:,tt+1)-munext(:,tt+1)); % update mean
        
        if nargout > 3
            zzcov_d1(:,:,tt) = zzcov_offdiag_convrg;
        end
    end
    
    % --------------------
    % Run Kalman smoother for time bins ttconverg to 1
    % (Beginning time bins where cov IS changing)
    for tt =  ttconvrg:-1:1
        Jt = (zzcov(:,:,tt)*mm.A')/Pnext(:,:,tt+1); % matrix we need
        zzcov(:,:,tt) = zzcov(:,:,tt) + Jt*(zzcov(:,:,tt+1)-Pnext(:,:,tt+1))*Jt'; % update cov
        zzmu(:,tt) = zzmu(:,tt) + Jt*(zzmu(:,tt+1)-munext(:,tt+1)); % update mean
        
        if nargout > 3
            zzcov_d1(:,:,tt) = Jt*zzcov(:,:,tt+1);
        end
    end
end

