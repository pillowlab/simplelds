function [zzmu,logli,zzcov,zzcov_d1] = runKalmanSmooth(dd,mm)
% [zzmu,logli,zzcov,zzcov_abovediag] = runKalmanSmooth(dd,mm)
%
% Run Kalman Filter-Smoother for latent LDS Gaussian model
%
% INPUTS:
% -------
%  dd [struct] - data structure with observations & inputs
%           .yy [ny x nT] - observations (aka emissions)
%           .uu [ns x nT] - inputs
%
%  mm [struct] - model struct with fields
%           .A [nz x nz] - dynamics matrix
%           .B [nz x ns] - input matrix (optional)
%           .C [ny x nz] - latents-to-observations matrix
%           .D [ny x ns] - input-to-observations matrix (optional)
%           .Q [nz x nz] - latent noise covariance
%           .R [ny x ny] - observed noise covariance
%           .Q0 [nz x nz] - latent noise covariance for 1st time step
%
% OUTPUTS:
% --------
%      zzmu [nz x nT]      - posterior mean E[zz | yy] from time 0 to T
%     logli [1 x 1]          - log-likelihood P( yy | ss, theta )
%     zzcov [nz x nz x nT] - marginal cov for time bins 0 to T
%  zzcov_d1 [nz x nz x nT-1]   - above-diagonal cov for bins 1 to T
%
%
% Basic equations:
% -----------------
% X_t = A*X_{t-1} + w_t,    w_t ~ N(0,Q)   % latent dynamics
% Y_t = C*X_t     + v_t,    v_t ~ N(0,R);  % observations

global logcy_glob

% Check that LDS model params are valid (covs are psd)
checkLDSparams(mm);

% Check that dynamics matrix is stable, if desired
% if max(abs(eig(mm.A)))>1
%     warning('Unstable dynamics matrix: largest eigenvalue of A matrix = %f\n', max(abs(eig(mm.A))));
% end

% Extract sizes
nz = size(mm.A,1);  % number of obs and latent dimensions
nT = size(dd.yy,2); % number of time bins

% pre-compute C'*inv(R) and C'*inv(R)*C;
CtRinv = mm.C'/mm.R;
CtRinvC = CtRinv*mm.C;

% check if input-latent matrix is provided
if isfield(mm,'B') && ~isempty(mm.B) 
    zin = mm.B*dd.uu;  % additive intput to latents (as column vectors)
else
    zin = zeros(nz,nT);
end

% check if intput-obs matrix is provided
if isfield(mm,'D') && ~isempty(mm.D) 
    yyctr = dd.yy-(mm.D*dd.uu);  % subtract additive intput to observations
else
    yyctr = dd.yy;
end

% Allocate storage for time bins 0 to T
zzmu = zeros(nz,nT);   % posterior mean E[ zz(t) | Y]
zzcov = zeros(nz,nz,nT); % marginal cov of zz(t)
munext = zeros(nz,nT);   % prior mean for next step:  E[ z(t) | y(1:t-1)]
Pnext = zeros(nz,nz,nT); % prior cov for next step: cov[ z(t) | y(1:t-1)]
logcy = zeros(1,nT); % store conditionals P(y(t) | y(1:t))

% ============================================
% Kalman Filter (forward pass)
% ============================================

% Process time bin 1
% ------------------
% 1. Predict
Pnext(:,:,1) = mm.Q0;   % prior cov for time bin 1
munext(:,1) = zeros(nz,1)+zin(:,1);  % prior mean for time bin 1
% 2. Update
zzcov(:,:,1) = inv(inv(Pnext(:,:,1))+CtRinvC); % KF posterior cov for time bin 1 
zzmu(:,1) = zzcov(:,:,1)*(CtRinv*yyctr(:,1) + (Pnext(:,:,1)\munext(:,1))); % KF mean for time bin 1
% 3. Compute marginal likelihood for bin 1
SigY = mm.C*Pnext(:,:,1)*mm.C'+mm.R; % conditional Y cov
logcy(1) = logmvnpdf(yyctr(:,1)',(mm.C*munext(:,1))',SigY); % marginal log P(y_1 )

% Process remaining time bins
% --------------------
for tt = 2:nT

    % Step 1 (Predict):
    Pnext(:,:,tt) = mm.A*zzcov(:,:,tt-1)*mm.A' + mm.Q; % prior cov for time bin t-1
    munext(:,tt) = mm.A*zzmu(:,tt-1)+zin(:,tt); % prior mean for time bin t-1
    % Step 2 (Update):
    zzcov(:,:,tt) = inv(inv(Pnext(:,:,tt))+CtRinvC);   % KF cov for time bin t-1
    zzmu(:,tt) = zzcov(:,:,tt)*(CtRinv*yyctr(:,tt) + (Pnext(:,:,tt)\munext(:,tt))); % KF mean
    
    % compute log P(y_t | y_{1:t-1})
    SigY = mm.C*Pnext(:,:,tt)*mm.C'+mm.R; % conditional Y cov
    logcy(tt) = logmvnpdf(yyctr(:,tt)',(mm.C*munext(:,tt))',SigY); % conditional log P(y_t | y_{1:t-1})
end

% compute marginal log-likelihood P(y | theta)
logli = sum(logcy);
logcy_glob = logcy;

% ============================================
% Kalman Smoother (backward pass)
% ============================================

if nargout > 3
    zzcov_d1 = zeros(nz,nz,nT-1); % above-diagonal covariance block
end

% Process time bins T-1 to 0
% --------------------
for tt = nT-1:-1:1
    Jt = (zzcov(:,:,tt)*mm.A')/Pnext(:,:,tt+1); % matrix we need
    zzcov(:,:,tt) = zzcov(:,:,tt) + Jt*(zzcov(:,:,tt+1)-Pnext(:,:,tt+1))*Jt'; % update cov
    zzmu(:,tt) = zzmu(:,tt) + Jt*(zzmu(:,tt+1)-munext(:,tt+1)); % update mean
    
    if nargout > 3
        zzcov_d1(:,:,tt) = Jt*zzcov(:,:,tt+1); % compute off-diagonal cov block
    end
end
