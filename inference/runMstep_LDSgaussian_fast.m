function mm = runMstep_LDSgaussian_fast(dd,mm,zzmu,Sig1,SigT,SigD0sum,SigD1sum,optsEM)
% mm = runMstep_LDSgaussian_fast(dd,mm,zzmu,Sig1,SigT,SigDsum,SigOD1sum,optsEM)
%
% Fast implementation of M-step updates for LDS-Gaussian model 
% Updates parameters: A, C, Q, R (not Q0)
%
%
% Inputs
% =======
%  dd [struct] - data structure with observations & inputs
%           .yy [ny x nT] - observations (aka emissions)
%           .uu [ns x nT] - inputs
%
%  mm [struct] - model structure with fields
%                 .A [nz x nz] - dynamics matrix
%                 .C [ny x nz] - latents-to-observations matrix
%                 .Q [nz x nz] - latent noise covariance
%                .Q0 [nz x nz] - latent noise covariance
%                 .R [ny x ny] - observation noise covariance
%
%       zzmu [nz x T]  - posterior mean of latents
%       Sig1 [nz x nz] - covariance over latents in 1st time bin
%       SigT [nz x nz] - covariance over latents in last time bin
%   SigD0sum [nz x nz] - summed diagonal blocks of latent covariance (including Sig1 and SigT)
%   SigD1sum [nz x nz] - summed above-diagonal block of covariance over latents
%
%  optsEM [struct] - optimization params (optional)
%       .maxiter - maximum # of iterations 
%       .dlogptol - stopping tol for change in log-likelihood 
%       .display - how often to report log-li
%       .update  - specify which params to update during M step
%
% Output
% =======
%  mmnew - new model struct with updated parameters

% Extract sizes
nt = size(zzmu,2);     % number of time bins

% =============== Update dynamics parameters ==============
if optsEM.update.Dynam

    % Compute sufficient statistics
    Mz1 = SigD0sum-SigT + zzmu(:,1:nt-1)*zzmu(:,1:nt-1)'; % E[zz*zz'] for 1 to T-1
    Mz2 = SigD0sum-Sig1 + zzmu(:,2:nt)*zzmu(:,2:nt)'; % E[zz*zz'] for 2 to T
    Mz12 = SigD1sum + (zzmu(:,1:nt-1)*zzmu(:,2:nt)');   % E[zz_t*zz_{t+1}'] (above-diag)

    % update dynamics matrix A
    if optsEM.update.A  
        Anew = Mz12'/Mz1;
        mm.A = Anew;
    end
    
    % Update noise covariance Q 
    if optsEM.update.Q
        Qnew = (Mz2 + mm.A*Mz1*mm.A' - mm.A*Mz12 - Mz12'*mm.A')/(nt-1);
        Qnew = (Qnew + Qnew')/2;  % symmetrize (to avoid numerical issues)
        mm.Q = Qnew;
    end

end

% =============== Update observation parameters ==============
if optsEM.update.Obs
    
    % Compute sufficient statistics
    if optsEM.update.Dynam
        Mz = Mz1 + SigT + zzmu(:,nt)*zzmu(:,nt)';  % re-use Mz1 if possible
    else
        Mz = SigD0sum + zzmu*zzmu'; % E[zz*zz']
    end
    Mzy = zzmu*dd.yy'; % E[zz*yy']
    
    % update obs matrix C
    if optsEM.update.C  
        Cnew = Mzy'/Mz;
        mm.C = Cnew;
    end
    
    % update obs noise covariance R
    if optsEM.update.R
        My = dd.yy*dd.yy';   % compute suff stat E[yy*yy']
        
        Rnew = (My + mm.C*Mz*mm.C' - mm.C*Mzy - Mzy'*mm.C')/nt;
        %Rnew = (Rnew + Rnew')/2;  % symmetrize
        mm.R = Rnew;

    end

end

