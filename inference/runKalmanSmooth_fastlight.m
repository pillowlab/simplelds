function [zzmu,logli,Sig1,SigT,SigDiag0Sum,SigDiag1Sum,ttconvrg] = runKalmanSmooth_fastlight(dd,mm,ttconvrg_guess)
% [zzmu,logli,Sig1,SigT,SigDiag0Sum,SigDiag1Sum,ttconvrg] = runKalmanSmooth_fastlight(yy,uu,mm,ttconvrg_guess)
%
% Run Kalman Filter-Smoother for latent LDS Gaussian model using fast
% method that skips updating the covariance once it has converged and only
% returns compressed covariance information needed by the M step
%
% INPUTS:
% -------
%  dd [struct] - data structure with observations & inputs
%           .yy [ny x nT] - observations (aka emissions)
%           .uu [ns x nT] - inputs
%
%  mm  [struct] - model struct with fields
%           .A [nz x nz] - dynamics matrix
%           .B [nz x ns] - input matrix (optional)
%           .C [ny x nz] - latents-to-observations matrix
%           .D [ny x ns] - input-to-observations matrix (optional)
%           .Q [nz x nz] - latent noise covariance
%           .R [ny x ny] - observed noise covariance
%           .Q0 [nz x nz] - latent noise covariance for 1st time step
%
% OUTPUTS:
% --------
%         zzmu [nz x nT] - posterior mean latents zz | yy
%        logli [1 x 1]   - log-likelihood P( yy | ss, theta )
%         Sig1 [nz x nz] - marginal cov for 1st time bin
%         SigT [nz x nz] - marginal cov for last time bin
%  SigDiag0Sum [nz x nz] - summed diagonal blocks of marginal cov
%  SigDiag1Sum [nz x nz] - summed above-diagonal blocks of marginal cov

% Check that LDS model params are valid (covs are psd)
checkLDSparams(mm);

% Set variable governing memory allocation, if not passed in
if nargin < 3
    ttconvrg_guess = 200;
end

% Extract sizes
[ny,nz] = size(mm.C);  % number of obs and latent dimensions
nT = size(dd.yy,2); % number of time bins

% pre-compute C'*inv(R) and C'*inv(R)*C;
CtRinv = mm.C'/mm.R;
CtRinvC = CtRinv*mm.C;

% check if input-latent matrix is provided
if isfield(mm,'B') && ~isempty(mm.B) 
    zin = mm.B*dd.uu;  % additive intput to latents (as column vectors)
else
    zin = zeros(nz,nT);
end

% check if intput-obs matrix is provided
if isfield(mm,'D') && ~isempty(mm.D) 
    yyctr = dd.yy-(mm.D*dd.uu);  % subtract additive intput to observations
else
    yyctr = dd.yy;
end

% ----  Allocate memory ----
zzmu = zeros(nz,nT);   % posterior mean E[ zz(t) | Y]
munext = zeros(nz,nT); % prior mean for next step:  E[ z(t) | y(1:t-1)]
logcy = zeros(1,nT); % store conditionals P(y(t) | y(1:t))

% Storage for initial block of covariances
zzcov = zeros(nz,nz,ttconvrg_guess); % marginal cov of zz(t)
Pnext = zeros(nz,nz,ttconvrg_guess); % prior cov for next step: cov[ z(t) | y(1:t-1)]

% ============================================
% Kalman Filter (forward pass through data)
% ============================================

% % --------------------
% % process 1st time bin
% zzcov(:,:,1) = inv(inv(mm.Q0)+CtRinvC);  
% zzmu(:,1) = zzcov(:,:,1)*(CtRinv*yyctr(:,1));  % NOTE: no inputs in first time bin
% logcy(1) = logmvnpdf(yyctr(:,1)',(mm.C*zin(:,1))',mm.C*mm.Q0*mm.C' + mm.R);

% --------------------
% Process 1st time bin
% --------------------
% 1. Predict
Pnext(:,:,1) = mm.Q0;   % prior cov for time bin 1
munext(:,1) = zeros(nz,1)+zin(:,1);  % prior mean for time bin 1
% 2. Update
zzcov(:,:,1) = inv(inv(Pnext(:,:,1))+CtRinvC); % KF posterior cov for time bin 1 
zzmu(:,1) = zzcov(:,:,1)*(CtRinv*yyctr(:,1) + (Pnext(:,:,1)\munext(:,1))); % KF mean for time bin 1
% 3. Compute marginal likelihood for bin 1
SigY = mm.C*Pnext(:,:,1)*mm.C'+mm.R; % conditional Y cov
logcy(1) = logmvnpdf(yyctr(:,1)',(mm.C*munext(:,1))',SigY); % marginal log P(y_1 )


% ---------------------------------------
% Process time bins where cov is changing
% ---------------------------------------
COVCONVRG = 0; % Boolean for when covariance has converged to its asymptotic value
tt = 1; % index variable
ttconvrg = nT; % initialize this variable

while tt<nT && ~COVCONVRG
    tt = tt+1; % update counter
    
    % Step 1 (Predict):
    Pnext(:,:,tt) = mm.A*zzcov(:,:,tt-1)*mm.A' + mm.Q; % prior cov for time bin t
    munext(:,tt) = mm.A*zzmu(:,tt-1)+zin(:,tt); % prior mean for time bin t
    % Step 2 (Update):
    zzcov(:,:,tt) = inv(inv(Pnext(:,:,tt))+CtRinvC);   % KF cov for time bin t
    zzmu(:,tt) = zzcov(:,:,tt)*(CtRinv*yyctr(:,tt) + (Pnext(:,:,tt)\munext(:,tt))); % KF mean
    
    % compute log P(y_t | y_{1:t-1})
    logcy(tt) = logmvnpdf(yyctr(:,tt)',(mm.C*munext(:,tt))',mm.C*Pnext(:,:,tt)*mm.C'+mm.R);
    
    if sum(abs(diag(zzcov(:,:,tt))-diag(zzcov(:,:,tt-1))))<1e-12
        COVCONVRG = 1;
        ttconvrg = tt;
        if ttconvrg > ttconvrg_guess
         fprintf('=== runKalmanSmooth_fastlight: consider increasing ttconvrg_guess ===\n');
         fprintf('ttconvrg=%d (guess=%d)\n',ttconvrg, ttconvrg_guess);
        end
    end
end

% ------------------------------------------------
% Process remaining time bins (where cov is fixed)
% ------------------------------------------------
Pnext_convrg = Pnext(:,:,ttconvrg); % prior covariance after convergence
Pnextinv = Pnext_convrg\eye(nz);  % inverse of Pnext after convergence
zzcov_convrg = zzcov(:,:,ttconvrg); % converged covariance
SigY = mm.C*Pnext_convrg*mm.C'+mm.R; % covariance for marginal likelihood
SigYlogdet = -0.5*logdet(2*pi*SigY);
SigYinv = SigY\eye(ny);
for tt = ttconvrg+1:nT
    
    % Step 1 (Predict):
    munext(:,tt) = mm.A*zzmu(:,tt-1)+zin(:,tt); % prior mean for time bin t
    % Step 2 (Update):
    zzmu(:,tt) = zzcov_convrg*(CtRinv*yyctr(:,tt) + (Pnextinv*munext(:,tt))); % KF mean
    
    % Compute log P(y_t | y_{1:t-1})
    mu = yyctr(:,tt)-(mm.C*munext(:,tt)); % mean
    logcy(tt) = SigYlogdet - 0.5*(mu'*SigYinv*mu);
end

% compute marginal log-likelihood P(y | theta)
logli = sum(logcy);

% covariance for final time bin
SigT = zzcov_convrg; 

% ============================================
% Kalman Smoother (backward pass)
% ============================================

if (ttconvrg > nT/2)
    % ---------------------------------------------------
    % Flag error if covariance did not converge during forward pass
    % ---------------------------------------------------

    warning('Warning: cov did not converge in time for fast method to be helpful (ttconvrg = %d)',ttconvrg);
    error('Switch to runKalmanSmooth.m / runEM_LDSgaussian.m instead');
end

% ---------------------------------------------------
% Run fast version, skipping updating cov updates in middle block
% ---------------------------------------------------

% -------------
% Final time bin
% -------------
Jt = (zzcov_convrg*mm.A')/Pnext_convrg; % matrix we need
zzcovtt = zzcov_convrg + Jt*(zzcov_convrg-Pnext_convrg)*Jt'; % update cov
zzmu(:,nT-1) = zzmu(:,nT-1) + Jt*(zzmu(:,nT)-munext(:,nT)); % update mean

SigDiag0Sum = zzcovtt + SigT;  % summed diagonal blocks of covariance
SigDiag1Sum = Jt*zzcov_convrg; % summed above-diagonal blocks of cov

% ---------------------------------------------------
% Kalman smooth final time bins where cov is changing
% ---------------------------------------------------

for tt = (nT-2):-1:(nT-ttconvrg)
    zzcovttp1 =zzcovtt;
    zzcovtt = zzcov_convrg + Jt*(zzcovttp1-Pnext_convrg)*Jt'; % update cov
    zzmu(:,tt) = zzmu(:,tt) + Jt*(zzmu(:,tt+1)-munext(:,tt+1)); % update mean
    
    SigDiag0Sum = SigDiag0Sum + zzcovtt; % summed diagonal blocks of covariance
    SigDiag1Sum = SigDiag1Sum+ Jt*zzcovttp1; % summed above-diagonal blocks of cov
end

% ---------------------------------------------------
% Kalman smooth middle time bins where cov ISN'T changing
% ---------------------------------------------------
zzcov_offdiag_convrg = Jt*zzcovtt; % converged off-diagonal cov
for tt = (nT-ttconvrg-1):-1:ttconvrg
    zzmu(:,tt) = zzmu(:,tt) + Jt*(zzmu(:,tt+1)-munext(:,tt+1)); % update mean
    
    SigDiag0Sum = SigDiag0Sum + zzcovtt; % summed diagonal blocks of covariance
    SigDiag1Sum = SigDiag1Sum+zzcov_offdiag_convrg; % summed above-diagonal blocks of cov
end

% ---------------------------------------------------
% Kalman smooth beginning time bins where cov IS changing
% ---------------------------------------------------
zzcov(:,:,ttconvrg) = zzcovtt;
for tt =  (ttconvrg-1):-1:1
    Jt = (zzcov(:,:,tt)*mm.A')/Pnext(:,:,tt+1); % matrix we need
    zzcov(:,:,tt) = zzcov(:,:,tt) + Jt*(zzcov(:,:,tt+1)-Pnext(:,:,tt+1))*Jt'; % update cov
    zzmu(:,tt) = zzmu(:,tt) + Jt*(zzmu(:,tt+1)-munext(:,tt+1)); % update mean
    
    SigDiag0Sum = SigDiag0Sum + zzcov(:,:,tt); % summed diagonal blocks of covariance
    SigDiag1Sum = SigDiag1Sum + Jt*zzcov(:,:,tt+1); % summed above-diagonal blocks of cov
    
end
Sig1 = zzcov(:,:,1); % Latent covariance for first time bin