function [logli,ttconvrg] = compLogMargLi_LDSgaussian(dd,mm)
% [logli,ttconvrg] = compLogMargLi_LDSgaussian(dd,mm)
%
% Run Kalman Filter for latent LDS Gaussian model in order to compute
% log marginal likelihood.  
%
% Uses fast method that skips updating the covariance after convergence
%
% INPUTS:
% -------
%  dd [struct] - data structure with observations & inputs
%           .yy [ny x nT] - observations (aka emissions)
%           .uu [ns x nT] - inputs
%
%  mm  [struct] - model struct with fields
%           .A [nz x nz] - dynamics matrix
%           .B [nz x ns] - input matrix (optional)
%           .C [ny x nz] - latents-to-observations matrix
%           .D [ny x ns] - input-to-observations matrix (optional)
%           .Q [nz x nz] - latent noise covariance
%           .R [ny x ny] - observations noise covariance
%           .Q0 [nz x nz] - latent noise covariance for 1st time step
%
% OUTPUTS:
% --------
%       logli - log-likelihood P( yy | ss, theta )
%    ttconvrg - time bin at which covariance has converged (to 1e-12)


% Extract sizes
[ny,nz] = size(mm.C);  % number of obs and latent dimensions
nT = size(dd.yy,2); % number of time bins

% pre-compute C'*inv(R) and C'*inv(R)*C;
CtRinv = mm.C'/mm.R;
CtRinvC = CtRinv*mm.C;

% check if input-latent matrix is provided
if isfield(mm,'B') && ~isempty(mm.B) 
    zin = mm.B*dd.uu;  % additive intput to latents (as column vectors)
else
    zin = zeros(nz,nT);
end

% check if intput-obs matrix is provided
if isfield(mm,'D') && ~isempty(mm.D) 
    yyctr = dd.yy-(mm.D*dd.uu);  % subtract additive intput to observations
else
    yyctr = dd.yy;
end

% ----  Allocate memory ----
logcy = zeros(1,nT); % store conditionals P(y(t) | y(1:t))

% ============================================
% Kalman Filter (forward pass through data)
% ============================================

% Process time bin 1
% ------------------
% 1. Predict
Pnext = mm.Q0;   % prior cov for time bin 1
munext = zeros(nz,1)+zin(:,1);  % prior mean for time bin 1
% 2. Update
zzcov = inv(inv(Pnext)+CtRinvC); % KF posterior cov for time bin 1 
zzmu = zzcov*(CtRinv*yyctr(:,1) + Pnext\munext); %#ok<MINV> % KF mean for time bin 1
% 3. Compute marginal likelihood for bin 1
SigY = mm.C*Pnext*mm.C'+mm.R; % conditional Y cov
logcy(1) = logmvnpdf(yyctr(:,1)',(mm.C*munext)',SigY); % marginal log P(y_1 )

% % --------------------
% % process 1st time bin
% zzcov = inv(inv(mm.Q0)+CtRinvC);  % covariance over latent
% zzmu = zzcov*(CtRinv*yyctr(:,1) + Pnext\munext); %#ok<MINV> % KF mean for time bin 1
% logcy(1) = logmvnpdf(yyctr(:,1)',(mm.C*zin(:,1))',mm.C*mm.Q0*mm.C' + mm.R);
% SigY = mm.C*Pnext*mm.C'+mm.R; % conditional Y cov
% logcy(1) = logmvnpdf(yyctr(:,1)',(mm.C*munext)',SigY); % marginal log P(y_1 )

% ----------------------------------------------
% NOTE: there was some bug in the fast version that I couldn't
% diagnose, so reverting to simple version where log-likelihood is computed
% recursively using standard Kalman filter forward pass.  
% ----------------------------------------------

% ------------------------------------------------
% Process all time bins. (orig: just initial bins (where cov is changing))
% ------------------------------------------------

%COVCONVRG = 0; % Boolean for when covariance has converged to its asymptotic value
%ttconvrg = inf; % initialize to infinity
tt = 1; % index variable

while tt<nT  % && ~COVCONVRG
    tt = tt+1; % update counter

    %zzcov_prev = zzcov;  % previous latent covariance
    
    % Step 1 (Predict):
    Pnext = mm.A*zzcov*mm.A' + mm.Q; % prior cov for time bin t
    munext = mm.A*zzmu+zin(:,tt); % prior mean for time bin t
    
    % Step 2 (Update):
    zzcov = inv(inv(Pnext)+CtRinvC);   % KF cov for time bin t
    zzmu = zzcov*(CtRinv*yyctr(:,tt) + (Pnext\munext)); %#ok<MINV> % KF mean
    
    % compute log P(y_t | y_{1:t-1})
    SigY = mm.C*Pnext*mm.C'+mm.R; % conditional Y cov
    ymu = mm.C*munext;
    logcy(tt) = logmvnpdf(yyctr(:,tt)',ymu',SigY);
    
%     if sum(abs(diag(zzcov)-diag(zzcov_prev)))<1e-12
%         COVCONVRG = 1; % BOOLEAN to indicate we converged
%         ttconvrg = tt; % time bin of convergence
%     end

end

% Compute log-li for entire dataset
logli = sum(logcy);

% % --------------------------------------------------------------------
% % Process remaining time bins (where cov is fixed) 
% % --------------------------------------------------------------------
% 
% if ttconvrg < nT
% 
%     % pre-cmpute some terms we need
%     Pnextinv = Pnext\eye(nz);  % inverse of Pnext after convergence
%     SigY = mm.C*Pnext*mm.C'+mm.R; % covariance for marginal likelihood
%     SigYlogdet = -0.5*logdet(2*pi*SigY); % log-determinant term in marginal likelihood
%     SigYinv = SigY\eye(ny); % inverse of marginal likelihood covariance
%     
%     % Process first time bin after convergence
%     tt = ttconvrg+1; % increment time index by 1
%     munext(:,tt) = mm.A*zzmu+zin(:,tt); % prior mean of Kalman filter
%     ymu = yyctr(:,tt)-(mm.C*munext(:,tt)); % conditional mean of observations
%     logcy(tt) = SigYlogdet - 0.5*(ymu'*SigYinv*ymu); % log conditional
% 
%     % Precompute linear update matrix and inputs
%     M = mm.A*zzcov*Pnextinv; % kalman filter matrix
%     zInput = mm.A*zzcov*CtRinv*yyctr(:,1:nT-1) + zin(:,2:nT); % relevant inputs
%     
%     % Process remaining time bins
%     ttindices = ttconvrg+2:nT;
%     for tt = ttindices
%         munext(:,tt) = M*munext(:,tt-1)+zInput(:,tt-1); % KF mean
%     end
%     ymu = yyctr(:,ttindices)-(mm.C*munext(:,ttindices)); % mean of observations
%     
%     % Compute log-likelihood terms P(y_t | y_{1:t-1}) for t > ttconvrg+1
%     logcy(ttindices) = SigYlogdet - 0.5*(sum(ymu.*(SigYinv*ymu)));
% 
% end
% 
% % Compute log-li for entire dataset
% logli = sum(logcy);

% -------------------------------------------------------------------
% Idea for going even faster: 
% Diagonalize M then convolve Krylov Matrices with zInput using FFT
% --------------
% starter code 
% --------------
% [U,S] = eig(M); % eigendecompose M
% S = diag(S);  % vector of eigenvalues
% Ui = inv(U); % inverse of eigenvalues
% tildemu0 = Ui*munextt(:,ttconvrg+1); % mean of next KF in Fourier 
% tildemunext = real(U*((S.^[1:nT-ttconvrg-1]).*tildemu0) + ...
%     <convolution of S^pow matrices with Zinput in Fourier domain> ; 
% ----------------------------------------------------------------------

