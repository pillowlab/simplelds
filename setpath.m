% setpaths.m
%
% Script to set paths for simpleLDS code

if ~exist('runKalmanSmooth','file')  % check if runEM_LDSgaussian is in path
    addpath inference;
end

if ~exist('sampleLDSgauss.m','file')  % check if function is in path
    addpath utils
end
