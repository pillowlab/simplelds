% demo_EMfitting_fast.m
%
% Illustrates newer fast and low-memory implementation of Kalman smoothing
% and EM fitting
%
% Samples from a latent Gaussian linear dynamical system (LDS) model, then
% run EM to estimate the model parameters

% Basic equations:
% -----------------
% X_t = A*X_{t-1} + eps_x,  eps_x ~ N(0,Q)  % latents
% Y_t = C*X_t + eps_y,      eps_y ~ N(0,R)  % observations
%
% With X_1 ~ N(0,Q0)    initial condition:  

setpath;

% Set dimensions
nz = 50;  % dimensionality of latent z
ny = 50; % dimensionality of observation y
nT = 2000; % number of time steps

% Set model parameters
% --------------------

% Generate random stable dynamics matrix A
A = randn(nz);
[u,s] = eig(A,'vector'); % get eigenvectors and eigenvals
s = s/max(abs(s))*.98; % set largest eigenvalue to lie inside unit circle (enforcing stability)
s(real(s)<0) = -s(real(s)<0); % set real parts to be positive (encouraging smoothness)
A = real(u*(diag(s)/u));  % reconstruct A from eigs and eigenvectors

% Set observation matrix C
C = 0.5*randn(ny,nz); % loading weights

% Dynamics noise covariance
Q = randn(nz); Q = .01*(Q'*Q+eye(nz)); % dynamics noise covariance
R = .02*diag(1*rand(ny,1)+.1); %  Y noise covariance

% Use discrete Lyapunov eq solver to compute asymptotic covariance of z
Pinf = dlyap(A,Q);
Q0 = Pinf; % set initial covariance to asymptotic value

%% Sample data from LDS model

mmtrue = struct('A',A,'C',C,'Q',Q,'R',R,'Q0',Q0);  % make param struct
[yy,zz] = sampleLDSgauss(mmtrue,nT); % sample from model

% Make data structure for fitting
dd = struct('yy',yy); 

%% Compute latents and log-marginal likelihood given true params

% Run Kalman Filter-Smoother to get posterior over latents given true data
fprintf('\nRunning standard Kalman Filter-Smoother...\n');
tic;
[zzmutrue,loglitrue,zzcovtrue] = runKalmanSmooth(dd,mmtrue);
toc;

% Compare speed of "fast Kalman Filter-Smoother"
fprintf('\nRunning fast Kalman Filter-Smoother...\n');
tic;
[zzmutrue1,loglitrue1] = runKalmanSmooth_fastlight(dd,mmtrue);
toc;

% Report log-li at true params
fprintf('\n Log-likelihood at true params: %.2f\n', loglitrue1);

%% Compute ML estimate for model params using EM

% Set options for EM     
optsEM.maxiter = 200;    % maximum # of iterations
optsEM.dlogptol = 1e-4;  % stopping tolerance
optsEM.display = 10;  % display frequency

% Specify which parameters to learn.  (Set to '0' or 'false' to NOT update).
optsEM.update.A = 1;
optsEM.update.C = 1; % DON'T UPDATE C if set to 0
optsEM.update.Q = 1;
optsEM.update.R = 1;

% Initialize fitting struct
mm0 = struct('A',A,'C',C,'Q',Q,'R',R,'Q0',Q0);  % make struct with initial params
if optsEM.update.A, mm0.A = eye(nz)*.1+randn(nz)*.01; end % initial A param
if optsEM.update.C, mm0.C = randn(ny,nz)*.1; end % initial C param
if optsEM.update.Q, mm0.Q = Q*2; end % initial Q param
if optsEM.update.R, mm0.R = R*2; end % initial R param

%% Run EM inference for model parameters

fprintf('\nRunning EM inference...\n');
tic;
[mm1,logliFinal1,logLiTrace1] = runEM_LDSgaussian_fast(dd,mm0,optsEM);
toc;

%% Report whether optimization succeeded in finding a posible global optimum
fprintf('\nLog-li at true params:      %.2f\n', loglitrue);
fprintf('Log-li at inferred params:  %.2f\n', logliFinal1);
% Report if we found the global optimum
if logliFinal1>=loglitrue, fprintf('(found better optimum -- SUCCESS!)\n');
else,   fprintf('(FAILED to find optimum!)\n');
end

nEM = length(logLiTrace1);
plot(2:nEM, logLiTrace1(2:end), [2 nEM], loglitrue*[1 1], 'k--');
xlabel('EM iteration'); 
ylabel('log P(Y | theta)');
title('EM performance');
